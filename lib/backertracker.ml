module StepStateStopSeq : sig
  type ('step, 'state, 'stop) node =
    | Nil of 'stop
    | Cons of 'step * ('step, 'state, 'stop) oddt

  and ('step, 'state, 'stop) t = unit -> ('step, 'state, 'stop) node
  and ('step, 'state, 'stop) oddnode = OddCons of 'state * ('step, 'state, 'stop) t
  and ('step, 'state, 'stop) oddt = unit -> ('step, 'state, 'stop) oddnode

  val cons2 : 'step -> 'state -> ('step, 'state, 'stop) t -> ('step, 'state, 'stop) t

  val append
    :  ('step, 'state, 'stop) t
    -> ('stop -> ('step, 'state, 'otherstop) t)
    -> ('step, 'state, 'otherstop) t

  val terminate : 'stop -> ('step, 'state, 'stop) t
  val iter : ('step -> unit) -> ('state -> unit) -> ('step, 'state, 'stop) t -> 'stop
  val map_state : ('s -> 't) -> ('step, 's, 'stop) t -> ('step, 't, 'stop) t
  val map_terminator : ('s -> 't) -> ('step, 'state, 's) t -> ('step, 'state, 't) t
  val to_seq : ('step, 'state, unit) t -> ('step * 'state) Seq.t
end = struct
  type ('step, 'state, 'stop) node =
    | Nil of 'stop
    | Cons of 'step * ('step, 'state, 'stop) oddt

  and ('step, 'state, 'stop) t = unit -> ('step, 'state, 'stop) node
  and ('step, 'state, 'stop) oddnode = OddCons of 'state * ('step, 'state, 'stop) t
  and ('step, 'state, 'stop) oddt = unit -> ('step, 'state, 'stop) oddnode

  let cons2 step state s () = Cons (step, fun () -> OddCons (state, s))

  let rec append s k () =
    match s () with
    | Nil b -> k b ()
    | Cons (a, s) ->
      Cons
        ( a
        , fun () ->
            match s () with
            | OddCons (b, s) -> OddCons (b, append s k) )
  ;;

  let terminate x () = Nil x

  let rec iter stepper stater s =
    match s () with
    | Nil v -> v
    | Cons (x, s) ->
      stepper x;
      (match s () with
       | OddCons (x, s) ->
         stater x;
         iter stepper stater s)
  ;;

  let rec map_state f s () =
    match s () with
    | Nil _ as v -> v
    | Cons (step, s) ->
      Cons
        ( step
        , fun () ->
            match s () with
            | OddCons (state, s) -> OddCons (f state, map_state f s) )
  ;;

  let rec map_terminator f s () =
    match s () with
    | Nil v -> Nil (f v)
    | Cons (x, s) ->
      Cons
        ( x
        , fun () ->
            match s () with
            | OddCons (x, s) -> OddCons (x, map_terminator f s) )
  ;;

  let rec to_seq s () =
    match s () with
    | Nil () -> Seq.Nil
    | Cons (x, s) ->
      (match s () with
       | OddCons (y, s) -> Seq.Cons ((x, y), to_seq s))
  ;;
end

module SSSSeq = StepStateStopSeq

module type SYMBOL = sig
  type t

  val none : t
  val is_none : t -> bool
  val all : t list
end

module type COORDINATE = sig
  type t

  val all : t list
end

module type BOARD = sig
  type t
  type symbol
  type coordinate

  val make : unit -> t
  val copy : t -> t
  val read : t -> coordinate -> symbol
  val write : t -> coordinate -> symbol -> unit
end

module type CONSTRAINTS = sig
  type symbol
  type coordinate
  type board

  val is_valid : board -> coordinate -> symbol -> bool
end

type event =
  | Forward
  | Backward
  | Sideways

module type SOLVER = sig
  type board
  type coordinate

  val solve : board -> (coordinate * event * board) Seq.t
end

module MakeBacktrack
    (Symbol : SYMBOL)
    (Coordinate : COORDINATE)
    (Board : BOARD with type symbol := Symbol.t and type coordinate := Coordinate.t)
    (Constraints : CONSTRAINTS
                   with type symbol := Symbol.t
                    and type coordinate := Coordinate.t
                    and type board := Board.t) :
  SOLVER with type board := Board.t and type coordinate := Coordinate.t = struct
  let ( .:[] ) = Board.read
  let ( .:[]<- ) = Board.write

  type ir = (Coordinate.t * event, unit, bool) SSSSeq.t

  let rec solve_board init working coordinates : ir =
    fun () ->
    match coordinates with
    | [] -> SSSSeq.terminate true ()
    | here :: there ->
      if not (Symbol.is_none init.:[here])
      then (
        assert (working.:[here] = init.:[here]);
        SSSSeq.cons2 (here, Forward) () (solve_board init working there) ())
      else solve_here init working here there Symbol.all ()

  and solve_here init working here there symbols : ir =
    fun () ->
    match symbols with
    | [] ->
      (* no more symbols, go back *)
      SSSSeq.Cons
        ( (here, Backward)
        , fun () ->
            working.:[here] <- Symbol.none;
            OddCons ((), SSSSeq.terminate false) )
    | this :: that ->
      if Constraints.is_valid working here this
      then
        SSSSeq.Cons
          ( (here, Forward)
          , fun () ->
              OddCons
                ( ()
                , fun () ->
                    working.:[here] <- this;
                    SSSSeq.append
                      (solve_board init working there)
                      (function
                        | true -> SSSSeq.terminate true
                        | false ->
                          fun () ->
                            SSSSeq.Cons
                              ( (here, Sideways)
                              , fun () ->
                                  OddCons ((), solve_here init working here there that) ))
                      () ) )
      else
        SSSSeq.Cons
          ( (here, Sideways)
          , fun () ->
              working.:[here] <- this;
              OddCons ((), solve_here init working here there that) )
  ;;

  let solve init =
    let working = Board.copy init in
    working, solve_board init working Coordinate.all
  ;;

  let solve init =
    let board, s = solve init in
    let s = SSSSeq.(to_seq @@ map_terminator ignore s) in
    Seq.memoize (Seq.map (fun ((coord, event), ()) -> coord, event, Board.copy board) s)
  ;;
end

type algo = Backtrack
