let stepper_kb =
  let open Notty.I in
  let a = Notty.A.empty in
  string a "[q]: quit" <-> string a "[space]: play/pause" <-> string a "[enter]: step"
;;

let auto_kb =
  let open Notty.I in
  let a = Notty.A.empty in
  string a "[q]: quit" <-> string a "[space]: play/pause"
;;

let wait_quit_kb =
  let open Notty.I in
  let a = Notty.A.empty in
  string a "[q]: quit"
;;

let with_keybindings img kb = Notty.I.( <-> ) img kb

let rec stepper_ui term img imgs =
  Notty_unix.Term.image term (with_keybindings img stepper_kb);
  match Notty_unix.Term.event term with
  | `End | `Key (`ASCII ('c' | 'C'), [ `Ctrl ]) | `Key (`ASCII ('q' | 'Q'), []) -> ()
  | `Key (`Enter, []) ->
    (match imgs () with
     | Seq.Nil -> wait_quit term img
     | Seq.Cons (img, imgs) -> stepper_ui term img imgs)
  | `Key (`ASCII ' ', _) -> auto_ui term img imgs
  | _ -> stepper_ui term img imgs

and auto_ui term img imgs =
  Notty_unix.Term.image term (with_keybindings img auto_kb);
  if Notty_unix.Term.pending term
  then (
    match Notty_unix.Term.event term with
    | `End | `Key (`ASCII ('c' | 'C'), [ `Ctrl ]) | `Key (`ASCII ('q' | 'Q'), []) -> ()
    | `Key (`ASCII ' ', _) -> stepper_ui term img imgs
    | _ -> auto_ui term img imgs)
  else (
    match
      Unix.sleepf 0.1;
      imgs ()
    with
    | Seq.Nil -> wait_quit term img
    | Seq.Cons (img, imgs) -> auto_ui term img imgs)

and wait_quit term img =
  Notty_unix.Term.image term (with_keybindings img wait_quit_kb);
  match Notty_unix.Term.event term with
  | `End | `Key (`ASCII ('c' | 'C'), [ `Ctrl ]) | `Key (`ASCII ('q' | 'Q'), []) -> ()
  | _ -> wait_quit term img
;;
