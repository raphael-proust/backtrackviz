let cartesian_product xs ys =
  List.fold_left (fun acc x -> List.fold_left (fun acc y -> (x, y) :: acc) acc ys) [] xs
;;

let rec lines_of_ic acc ic =
  match input_line ic with
  | exception End_of_file -> Array.of_list (List.rev acc)
  | l -> lines_of_ic (l :: acc) ic
;;

let lines_of_file name =
  let ic = open_in name in
  Fun.protect ~finally:(fun () -> close_in ic) (fun () -> lines_of_ic [] ic)
;;

let size = 4 (* TODO: place in `conf.ml` file *)
let () = assert (size = 4 || size = 9)
let sqrtsize = if size = 4 then 2 else if size = 9 then 3 else assert false

module Symbol : sig
  include Backertracker.SYMBOL

  val of_char : int -> char -> t
  val pp : Format.formatter -> t -> unit
  val render : Notty.A.t -> t -> Notty.I.t
end = struct
  type t = int

  let none = 0
  let all = List.init size succ

  let is_none = function
    | 0 -> true
    | _ -> false
  ;;

  let pp fmt t = if t = none then Format.fprintf fmt " " else Format.fprintf fmt "%d" t

  let render a t =
    if t = none then Notty.I.string a " " else Notty.I.string a (string_of_int t)
  ;;

  let of_char = function
    | ' ' -> none
    | '1' -> 1
    | '2' -> 2
    | '3' -> 3
    | '4' -> 4
    | '5' -> 5
    | '6' -> 6
    | '7' -> 7
    | '8' -> 8
    | '9' -> 9
    | _ -> failwith "unknown symbol"
  ;;

  let of_char size c =
    let s = of_char c in
    if s > size then failwith "symbol exceeds limit";
    s
  ;;
end

module Coordinate : sig
  type offset = private int

  val offsets : offset list

  type t =
    { vertical : offset
    ; horizontal : offset
    }

  val all : t list
  val pp_coordinates : Format.formatter -> t -> unit

  type region = private int

  val region_of_coordinates : t -> region
  val coordinates_of_region : region -> t list
end = struct
  type offset = int

  let offsets = List.init size Fun.id

  type t =
    { vertical : offset
    ; horizontal : offset
    }

  let all =
    List.map
      (fun (vertical, horizontal) -> { vertical; horizontal })
      (cartesian_product offsets offsets)
  ;;

  let pp_coordinates fmt { vertical; horizontal } =
    Format.fprintf fmt "%d-%d" vertical horizontal
  ;;

  type region = int

  let region_of_coordinates { vertical; horizontal } =
    let low = horizontal / sqrtsize in
    let high = vertical / sqrtsize in
    (high * sqrtsize) + low
  ;;

  let region_map =
    Array.init size (fun vertical ->
      Array.init size (fun horizontal -> region_of_coordinates { vertical; horizontal }))
  ;;

  let region_of_coordinates { vertical; horizontal } = region_map.(vertical).(horizontal)

  let coordinates_of_region_map =
    Array.init size (fun region ->
      List.filter (fun coordinate -> region_of_coordinates coordinate = region) all)
  ;;

  let coordinates_of_region region = coordinates_of_region_map.(region)
end

module Board : sig
  include
    Backertracker.BOARD with type symbol := Symbol.t and type coordinate := Coordinate.t

  val of_file : string -> t
  val pp : Coordinate.t -> int -> Format.formatter -> t -> unit
  val render : t -> Notty.I.t
end = struct
  type t = Symbol.t array array

  let of_file name =
    let lines = lines_of_file name in
    let size = Array.length lines in
    if not (size = 4 || size = 9) then failwith "unsupported size";
    let proto_board =
      Array.map
        (fun l -> l |> String.to_seq |> Seq.map (Symbol.of_char size) |> Array.of_seq)
        lines
    in
    if Array.exists (fun a -> Array.length a <> size) proto_board
    then failwith "irregularly shaped board";
    proto_board
  ;;

  let make () =
    Array.init size (fun _vertical -> Array.init size (fun _horizontal -> Symbol.none))
  ;;

  let copy t =
    Array.init size (fun vertical ->
      Array.init size (fun horizontal -> t.(vertical).(horizontal)))
  ;;

  let read t { Coordinate.vertical; horizontal } =
    t.((vertical :> int)).((horizontal :> int))
  ;;

  let write t { Coordinate.vertical; horizontal } symbol =
    t.((vertical :> int)).((horizontal :> int)) <- symbol
  ;;

  let symbol_pp _ _ _ fmt s = Symbol.pp fmt s

  let pp4 coord indent fmt t =
    match t with
    | [| [| s00; s01; s02; s03 |]
       ; [| s10; s11; s12; s13 |]
       ; [| s20; s21; s22; s23 |]
       ; [| s30; s31; s32; s33 |]
      |] ->
      Format.fprintf
        fmt
        "%s┏━┯━┳━┯━┓\n\
         %s┃%a│%a┃%a│%a┃\n\
         %s┠─┼─╂─┼─┨\n\
         %s┃%a│%a┃%a│%a┃\n\
         %s┣━┿━╋━┿━┫\n\
         %s┃%a│%a┃%a│%a┃\n\
         %s┠─┼─╂─┼─┨\n\
         %s┃%a│%a┃%a│%a┃\n\
         %s┗━┷━┻━┷━┛\n"
        indent
        indent
        (symbol_pp coord 0 0)
        s00
        (symbol_pp coord 0 1)
        s01
        (symbol_pp coord 0 2)
        s02
        (symbol_pp coord 0 3)
        s03
        indent
        indent
        (symbol_pp coord 1 0)
        s10
        (symbol_pp coord 1 1)
        s11
        (symbol_pp coord 1 2)
        s12
        (symbol_pp coord 1 3)
        s13
        indent
        indent
        (symbol_pp coord 2 0)
        s20
        (symbol_pp coord 2 1)
        s21
        (symbol_pp coord 2 2)
        s22
        (symbol_pp coord 2 3)
        s23
        indent
        indent
        (symbol_pp coord 3 0)
        s30
        (symbol_pp coord 3 1)
        s31
        (symbol_pp coord 3 2)
        s32
        (symbol_pp coord 3 3)
        s33
        indent
    | _ -> assert false
  ;;

  let pp9 coord indent fmt t =
    ignore coord;
    (* TODO *)
    match t with
    | [| [| s00; s01; s02; s03; s04; s05; s06; s07; s08 |]
       ; [| s10; s11; s12; s13; s14; s15; s16; s17; s18 |]
       ; [| s20; s21; s22; s23; s24; s25; s26; s27; s28 |]
       ; [| s30; s31; s32; s33; s34; s35; s36; s37; s38 |]
       ; [| s40; s41; s42; s43; s44; s45; s46; s47; s48 |]
       ; [| s50; s51; s52; s53; s54; s55; s56; s57; s58 |]
       ; [| s60; s61; s62; s63; s64; s65; s66; s67; s68 |]
       ; [| s70; s71; s72; s73; s74; s75; s76; s77; s78 |]
       ; [| s80; s81; s82; s83; s84; s85; s86; s87; s88 |]
      |] ->
      Format.fprintf
        fmt
        "%s┏━┯━┯━┳━┯━┯━┳━┯━┯━┓\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┠─┼─┼─╂─┼─┼─╂─┼─┼─┨\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┠─┼─┼─╂─┼─┼─╂─┼─┼─┨\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┣━┿━┿━╋━┿━┿━╋━┿━┿━┫\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┠─┼─┼─╂─┼─┼─╂─┼─┼─┨\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┠─┼─┼─╂─┼─┼─╂─┼─┼─┨\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┣━┿━┿━╋━┿━┿━╋━┿━┿━┫\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┠─┼─┼─╂─┼─┼─╂─┼─┼─┨\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┠─┼─┼─╂─┼─┼─╂─┼─┼─┨\n\
         %s┃%a│%a│%a┃%a│%a│%a┃%a│%a│%a┃\n\
         %s┗━┷━┷━┻━┷━┷━┻━┷━┷━┛\n"
        indent
        indent
        Symbol.pp
        s00
        Symbol.pp
        s01
        Symbol.pp
        s02
        Symbol.pp
        s03
        Symbol.pp
        s04
        Symbol.pp
        s05
        Symbol.pp
        s06
        Symbol.pp
        s07
        Symbol.pp
        s08
        indent
        indent
        Symbol.pp
        s10
        Symbol.pp
        s11
        Symbol.pp
        s12
        Symbol.pp
        s13
        Symbol.pp
        s14
        Symbol.pp
        s15
        Symbol.pp
        s16
        Symbol.pp
        s17
        Symbol.pp
        s18
        indent
        indent
        Symbol.pp
        s20
        Symbol.pp
        s21
        Symbol.pp
        s22
        Symbol.pp
        s23
        Symbol.pp
        s24
        Symbol.pp
        s25
        Symbol.pp
        s26
        Symbol.pp
        s27
        Symbol.pp
        s28
        indent
        indent
        Symbol.pp
        s30
        Symbol.pp
        s31
        Symbol.pp
        s32
        Symbol.pp
        s33
        Symbol.pp
        s34
        Symbol.pp
        s35
        Symbol.pp
        s36
        Symbol.pp
        s37
        Symbol.pp
        s38
        indent
        indent
        Symbol.pp
        s40
        Symbol.pp
        s41
        Symbol.pp
        s42
        Symbol.pp
        s43
        Symbol.pp
        s44
        Symbol.pp
        s45
        Symbol.pp
        s46
        Symbol.pp
        s47
        Symbol.pp
        s48
        indent
        indent
        Symbol.pp
        s50
        Symbol.pp
        s51
        Symbol.pp
        s52
        Symbol.pp
        s53
        Symbol.pp
        s54
        Symbol.pp
        s55
        Symbol.pp
        s56
        Symbol.pp
        s57
        Symbol.pp
        s58
        indent
        indent
        Symbol.pp
        s60
        Symbol.pp
        s61
        Symbol.pp
        s62
        Symbol.pp
        s63
        Symbol.pp
        s64
        Symbol.pp
        s65
        Symbol.pp
        s66
        Symbol.pp
        s67
        Symbol.pp
        s68
        indent
        indent
        Symbol.pp
        s70
        Symbol.pp
        s71
        Symbol.pp
        s72
        Symbol.pp
        s73
        Symbol.pp
        s74
        Symbol.pp
        s75
        Symbol.pp
        s76
        Symbol.pp
        s77
        Symbol.pp
        s78
        indent
        indent
        Symbol.pp
        s80
        Symbol.pp
        s81
        Symbol.pp
        s82
        Symbol.pp
        s83
        Symbol.pp
        s84
        Symbol.pp
        s85
        Symbol.pp
        s86
        Symbol.pp
        s87
        Symbol.pp
        s88
        indent
    | _ -> assert false
  ;;

  let pp coord indent fmt t =
    let indent = String.make (indent * sqrtsize) ' ' in
    if size = 4
    then pp4 coord indent fmt t
    else if size = 9
    then pp9 coord indent fmt t
    else assert false
  ;;

  let render t =
    assert (size = 4);
    Renderer.board4 Symbol.render t
  ;;
end

(* board access syntax *)
let ( .:[] ) = Board.read
let ( .:[]<- ) = Board.write

module Constraints : sig
  include
    Backertracker.CONSTRAINTS
    with type symbol := Symbol.t
     and type coordinate := Coordinate.t
     and type board := Board.t
end = struct
  let is_valid_vertically board ~horizontal value =
    List.for_all
      (fun vertical -> value != board.:[{ vertical; horizontal }])
      Coordinate.offsets
  ;;

  let is_valid_horizontally board ~vertical value =
    List.for_all
      (fun horizontal -> value != board.:[{ vertical; horizontal }])
      Coordinate.offsets
  ;;

  let is_valid_squarelly board ~vertical ~horizontal value =
    List.for_all
      (fun coordinate -> value != board.:[coordinate])
      (Coordinate.coordinates_of_region
         (Coordinate.region_of_coordinates { vertical; horizontal }))
  ;;

  let is_valid board { Coordinate.vertical; horizontal } value =
    is_valid_vertically board ~horizontal value
    && is_valid_horizontally board ~vertical value
    && is_valid_squarelly board ~vertical ~horizontal value
  ;;
end

module type SOLVER =
  Backertracker.SOLVER with type board := Board.t and type coordinate := Coordinate.t

module Backtrack : SOLVER =
  Backertracker.MakeBacktrack (Symbol) (Coordinate) (Board) (Constraints)
