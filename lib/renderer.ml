(* TODO: currently assumes that symbols take 1 space *)

(* TODO: inverse style for focus *)

let a = Notty.A.empty

let line symbol = function
  | [| s0; s1; s2; s3 |] ->
    let open Notty.I in
    let a = Notty.A.empty in
    string a "┃"
    <|> symbol a s0
    <|> string a "│"
    <|> symbol a s1
    <|> string a "┃"
    <|> symbol a s2
    <|> string a "│"
    <|> symbol a s3
    <|> string a "┃"
  | _ -> assert false
;;

let board4 symbol = function
  | [| [| _; _; _; _ |] as l0
     ; [| _; _; _; _ |] as l1
     ; [| _; _; _; _ |] as l2
     ; [| _; _; _; _ |] as l3
    |] ->
    let open Notty.I in
    let a = Notty.A.empty in
    let line = line symbol in
    string a "┏━┯━┳━┯━┓"
    <-> line l0
    <-> string a "┠─┼─╂─┼─┨"
    <-> line l1
    <-> string a "┣━┿━╋━┿━┫"
    <-> line l2
    <-> string a "┠─┼─╂─┼─┨"
    <-> line l3
    <-> string a "┗━┷━┻━┷━┛"
  | _ -> assert false
;;
