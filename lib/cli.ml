open Cmdliner

let init =
  let doc = "file holding the initial board (empty if none)" in
  Arg.(value & opt (some file) None & info [ "initial-board" ] ~docv:"INIT" ~doc)
;;

let algo =
  let doc = "algorithm to use for solving" in
  Arg.(
    value
    & opt (enum [ "backtrack", Backertracker.Backtrack ]) Backertracker.Backtrack
    & info [ "algorithm" ] ~docv:"ALGORITHM" ~doc)
;;
