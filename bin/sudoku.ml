open Backtrackviz

let pick_algo : Backertracker.algo -> (module Solving.SOLVER) = function
  | Backtrack -> (module Solving.Backtrack)
;;

let sudoku init algo =
  let board =
    match init with
    | None -> Solving.Board.make ()
    | Some name -> Solving.Board.of_file name
  in
  let img = Solving.Board.render board in
  let (module Solver : Solving.SOLVER) = pick_algo algo in
  let steps = Solver.solve board in
  let imgs = Seq.map (fun (_, _, b) -> Solving.Board.render b) steps in
  let term = Notty_unix.Term.create () in
  Fun.protect
    ~finally:(fun () -> Notty_unix.Term.release term)
    (fun () -> Ui.stepper_ui term img imgs)
;;

let sudoku_term = Cmdliner.Term.(const sudoku $ Cli.init $ Cli.algo)

let sudoku_cmd =
  let doc = "sudoku: solve a sudoku grid" in
  let info = Cmdliner.Cmd.info "sudoku" ~version:"dev" ~doc in
  Cmdliner.Cmd.v info sudoku_term
;;

let () = exit (Cmdliner.Cmd.eval sudoku_cmd)
